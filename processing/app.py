import datetime
import json
import logging
import logging.config
import json, os
import uuid

import connexion
import requests
import yaml
from apscheduler.schedulers.background import BackgroundScheduler
from connexion import NoContent
from flask_cors import CORS, cross_origin

body = {}
MAX_EVENTS = 12
filename = 'data.json'
YAML_FILE = 'sajjin-military-1.0.0-resolved.yaml'

with open('log_conf.yml', 'r') as f:
    log_config = yaml.safe_load(f.read())
    logging.config.dictConfig(log_config)

with open('app_conf.yml', 'r') as f:
    app_config = yaml.safe_load(f.read())

logger = logging.getLogger('basicLogger')

def get_military_stats():
    logger.info("Received event get_stats request {}".format(uuid.uuid4()))
    try:
        with open(app_config['datastore']['filename'], 'r') as file:
            file_data = file.read()
            logger.debug("Loaded statistics: {}".format(json.loads(file_data)))
            logger.info("get_stats request has been completed")
            return json.loads(file_data), 200
    except:
        logger.error("Statistic file cannot be found!")
        return "Statistics not exist", 404

def populate_stats():
    """ Periodically update stats """
    global most_gp_type
    logger.info("Start Periodic Processing")
    try:
        with open(app_config['datastore']['filename'], 'r') as file:
            stats = json.loads(file.read())
    except:
        with open(app_config['datastore']['filename'], 'w') as file:
            file.write(json.dumps(
                {"most_military_force_used": "Navy", "num_of_countries": 0, "num_of_operations": 0,
                 "last_updated": datetime.datetime.strftime(datetime.datetime.now(), "%Y-%m-%dT%H:%M:%SZ")}))

    now = datetime.datetime.strftime(datetime.datetime.now(), "%Y-%m-%dT%H:%M:%SZ")
    mc_request = requests.get(app_config['get_military_country']['url'] + "?start_timestamp=" + stats[
        'last_updated'] + "&end_timestamp=" + now)
    mo_request = requests.get(app_config['get_military_operation']['url'] + "?start_timestamp=" + stats[
        'last_updated'] + "&end_timestamp=" + now)
    if mc_request.status_code != 200:
        logger.error("ERROR ON Receiving data for military.")
    else:
        logger.info("Successfully received military.")
    if mo_request.status_code != 200:
        logger.error("ERROR ON Receiving data for military.")
    else:
        logger.info("Successfully received military.")
    mo_data = json.loads(mo_request.content)
    mc_data = json.loads(mc_request.content)
    mo_len = len(mo_data) + stats['num_of_countries']
    mc_len = len(mc_data) + stats['num_of_operations']
    gp_type_list = []

    for gp in mc_data:
        gp_type_list.append(gp['military_force_name'])
    if len(gp_type_list) > 0:
        most_gp_type = max(gp_type_list)

    data_obj = {"most_military_force_used": most_gp_type, "num_of_countries": mc_len, "num_of_operations": mo_len,
                "last_updated": now}

    with open(app_config['datastore']['filename'], 'w') as file:
        file.write(json.dumps(data_obj))
    logger.debug("Successfully saved the new stats: {}".format(data_obj))


def init_scheduler():
    sched = BackgroundScheduler(daemon=True)
    sched.add_job(populate_stats,
                  'interval', seconds=app_config
        ['scheduler']['period_sec'])
    sched.start()


app = connexion.FlaskApp(__name__, specification_dir='')
CORS(app.app)
app.app.config['CORS_HEADERS'] = 'Content-Type'
app.add_api(YAML_FILE, strict_validation=True, validate_responses=True)

if __name__ == "__main__":
    init_scheduler()
    app.run(port=8100, use_reloader=False)