import connexion
from connexion import NoContent
import json, logging.config, requests, logging, yaml
import os
import uuid
from flask import Response
from pykafka import KafkaClient
from datetime import datetime
from flask_cors import CORS, cross_origin

with open('log_conf.yml', 'r') as f:
    log_config = yaml.safe_load(f.read())
    logging.config.dictConfig(log_config)

with open('app_conf.yml', 'r') as f:
    app_config = yaml.safe_load(f.read())

logger = logging.getLogger('basicLogger')


def get_military_operation(index):
    hostname = "%s:%d" % (app_config["events"]["hostname"],
                          app_config["events"]["port"])
    client = KafkaClient(hosts=hostname)
    topic = client.topics[str.encode(app_config["events"]["topic"])]
    consumer = topic.get_simple_consumer(reset_offset_on_start=True, consumer_timeout_ms=1000)
    my_ls = []
    logger.info("Retrieving BP at index %d" % index)
    try:
        for msg in consumer:
            msg_str = msg.value.decode('utf-8')
            msg = json.loads(msg_str)
            if msg["type"] == "operation":
                my_ls.append(msg['payload'])
        if len(my_ls) > index:
            event = my_ls[index]
            return event, 200

    except:
        logger.error("No more messages found")
    logger.error("Could not find BP at index %d" % index)
    return {"message": "Not Found"}, 404


def get_military_country_of_origin(index):
    hostname = "%s:%d" % (app_config["events"]["hostname"],
                          app_config["events"]["port"])
    client = KafkaClient(hosts=hostname)
    topic = client.topics[str.encode(app_config["events"]["topic"])]
    consumer = topic.get_simple_consumer(reset_offset_on_start=True, consumer_timeout_ms=1000)
    my_ls = []
    logger.info("Retrieving BP at index %d" % index)
    try:
        for msg in consumer:
            msg_str = msg.value.decode('utf-8')
            msg = json.loads(msg_str)
            # Find the event at the index you want and # return code 200
            # i.e., return event, 200
            if msg["type"] == "country":
                my_ls.append(msg['payload'])
        if len(my_ls) > index:
            event = my_ls[index]
            return event, 200

    except:
        logger.error("No more messages found")

    logger.error("Could not find BP at index %d" % index)
    return {"message": "Not Found"}, 404


app = connexion.FlaskApp(__name__, specification_dir='')
CORS(app.app)
app.app.config['CORS_HEADERS'] = 'Content-Type'
app.add_api("sajjin-military-1.0.0-resolved.yaml", strict_validation=True, validate_responses=True)

if __name__ == "__main__":
    app.run(port=8200, debug=True)
