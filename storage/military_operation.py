from sqlalchemy import Column, Integer, String, DateTime
from base import Base
import datetime


class MilitaryOperation(Base):
    """ Military Operation """

    __tablename__ = "military_operation"

    id = Column(Integer, primary_key=True)
    operation_id = Column(String(250), nullable=False)
    operation_unit_name = Column(String(100), nullable=False)
    country_name_full = Column(String(250), nullable=False)
    operation_country_name = Column(String(100), nullable=False)
    date_created = Column(DateTime, nullable=False)

    def __init__(self, operation_id, operation_unit_name, country_name_full, operation_country_name):
        """ Initializes a operations """
        self.operation_id = operation_id
        self.operation_unit_name = operation_unit_name
        self.country_name_full = country_name_full
        self.operation_country_name = operation_country_name
        self.date_created = datetime.datetime.now()  # Sets the date/time record is created


def to_dict(self):
    """ Dictionary Representation of a operation """

    dict = {}
    dict['id'] = self.id
    dict['operation_id'] = self.operation_id
    dict['operation_unit_name'] = self.operation_unit_name
    dict['country_name_full'] = self.country_name_full
    dict['operation_country_name'] = self.operation_country_name
    dict['date_created'] = self.date_created

    return dict
