import mysql.connector
#pip install mysql-connector-python

db_conn = mysql.connector.connect(host="acit3855sajjin.eastus2.cloudapp.azure.com",port="3306", user="root", password="password", db="events")
db_cursor = db_conn.cursor()

db_cursor.execute('''
CREATE TABLE military_country
(id INT NOT NULL AUTO_INCREMENT,
military_id VARCHAR(250) NOT NULL,
country_name_full VARCHAR(250) NOT NULL,
country_name_short VARCHAR(2) NOT NULL,
military_force_name VARCHAR(100) NOT NULL,
date_created VARCHAR(100) NOT NULL,
updated_at VARCHAR(100),
PRIMARY KEY (id))
''')

db_cursor.execute('''
CREATE TABLE military_operation
(id INT NOT NULL AUTO_INCREMENT,
operation_id VARCHAR(250) NOT NULL,
operation_unit_name VARCHAR(250) NOT NULL,
country_name_full VARCHAR(250) NOT NULL,
operation_country_name VARCHAR(100) NOT NULL,
date_created VARCHAR(100) NOT NULL,
updated_at VARCHAR(100),
CONSTRAINT id_pk PRIMARY KEY (id))
''')

db_conn.commit()
db_conn.close()
