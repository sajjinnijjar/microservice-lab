from sqlalchemy import Column, Integer, String, DateTime
from base import Base
import datetime


class MilitaryCountry(Base):
    """ Military Country """

    __tablename__ = "military_country"

    id = Column(Integer, primary_key=True)
    military_id = Column(String(250), nullable=False)
    country_name_full = Column(String(250), nullable=False)
    country_name_short = Column(String(2), nullable=False)
    military_force_name = Column(String(100), nullable=False)
    date_created = Column(String(100), nullable=True)
    updated_at = Column(String(100), nullable=True)

    def __init__(self, military_id, country_name_full, country_name_short, military_force_name):
        """ Initializes a Military Country """
        self.military_id = military_id
        self.country_name_full = country_name_full
        self.country_name_short = country_name_short
        self.military_force_name = military_force_name
        self.date_created = datetime.datetime.now()  # Sets the date/time record is created
        self.updated_at = datetime.datetime.now()

    def to_dict(self):
        """ Dictionary Representation of a Military Country """
        dict = {}
        dict['id'] = self.id
        dict['military_id'] = self.military_id
        dict['country_name_full'] = self.country_name_full
        dict['country_name_short'] = self.country_name_short
        dict['military_force_name'] = self.military_force_name
        dict['date_created'] = self.date_created
        dict['updated_at'] = self.updated_at

        return dict
