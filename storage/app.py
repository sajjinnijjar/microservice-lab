from time import sleep

import connexion
from connexion import NoContent
from flask import Response
from sqlalchemy import create_engine, and_
from sqlalchemy.orm import sessionmaker
from base import Base
import datetime, yaml, logging, logging.config, json
import mysql.connector
from pykafka.common import OffsetType
from threading import Thread
from pykafka import KafkaClient

from military_country import MilitaryCountry
from military_operation import MilitaryOperation

yaml_file = 'sajjin-military-1.0.0-resolved.yaml'

with open('log_conf.yml', 'r') as f:
    log_config = yaml.safe_load(f.read())
    logging.config.dictConfig(log_config)

logger = logging.getLogger('basicLogger')

host = "acit3855sajjin.eastus2.cloudapp.azure.com"
with open('app_conf.yml', 'r') as f:
    app_config = yaml.safe_load(f.read())
    logger.info(f"Connecting to DB. Hostname {host}, Port: 3306")

DB_ENGINE = create_engine(
    f'mysql+pymysql://{app_config["datastore"]["user"]}:{app_config["datastore"]["password"]}@{app_config["datastore"]["hostname"]}:{app_config["datastore"]["port"]}/{app_config["datastore"]["db"]}')
Base.metadata.bind = DB_ENGINE
DB_SESSION = sessionmaker(bind=DB_ENGINE)
logger.info("Connecting to DB. Hostname:{} ,Port:{}".format(app_config['datastore']['hostname'],
                                                            app_config['datastore']['port']))


def get_military_country_of_origin(start_timestamp, end_timestamp):
    session = DB_SESSION()
    start_timestamp_datetime = datetime.datetime.strptime(start_timestamp, "%Y-%m-%dT%H:%M:%SZ")
    end_timestamp_datetime = datetime.datetime.strptime(end_timestamp, "%Y-%m-%dT%H:%M:%SZ")
    readings = session.query(MilitaryCountry).filter(
        and_(MilitaryCountry.date_created >= start_timestamp_datetime, MilitaryCountry.date_created < end_timestamp_datetime))
    results_list = []
    for reading in readings:
        a = reading.__dict__
        del a["_sa_instance_state"]
        results_list.append(a)
    session.close()
    logger.info("Query for Military Country after %s returns %d results" % (start_timestamp, len(results_list)))
    return Response(response=json.dumps(results_list), status=200, headers={'Content-type': 'application/json'})


def get_military_operation(start_timestamp, end_timestamp):
    session = DB_SESSION()
    start_timestamp_datetime = datetime.datetime.strptime(start_timestamp, "%Y-%m-%dT%H:%M:%SZ")
    end_timestamp_datetime = datetime.datetime.strptime(end_timestamp, "%Y-%m-%dT%H:%M:%SZ")
    readings = session.query(MilitaryOperation).filter(
        and_(MilitaryOperation.date_created >= start_timestamp_datetime,
             MilitaryOperation.date_created < end_timestamp_datetime))
    results_list = []
    for reading in readings:
        a = reading.__dict__
        del a["_sa_instance_state"]
        results_list.append(a)
    session.close()
    logger.info("Query for Member readings after %s returns %d results" % (start_timestamp, len(results_list)))
    return Response(response=json.dumps(results_list), status=200, headers={'Content-type': 'application/json'})


def report_military_country_of_origin(body):
    """ Receives military country of origin"""
    # Implement Here
    session = DB_SESSION()

    bp = MilitaryCountry(
        body['military_id'],
        body['country_name_full'],
        body['country_name_short'],
        body['military_force_name'])

    session.add(bp)

    session.commit()
    session.close()

    logger.debug(f"military_country response Id: {body['military_id']}")


def report_military_operation(body):
    """ Receives military operation"""
    # Implement Here
    session = DB_SESSION()

    bp = MilitaryOperation(
        body['operation_id'],
        body['operation_unit_name'],
        body['country_name_full'],
        body['operation_country_name'])

    session.add(bp)

    session.commit()
    session.close()

    logger.debug(f"military_operation response Id: {body['operation_id']}")


def process_messages():
    """ Process event messages """
    logger.info('Processing event messages started')
    hostname = "%s:%d" % (app_config["events"]["hostname"],
                          app_config["events"]["port"])
    max_tries = int(app_config["events"]["max_tries"])
    trying = 0
    while trying < max_tries:
        try:
            logger.info('Connecting to Kafka. Try #:{}'.format(trying))
            client = KafkaClient(hosts=hostname)
            topic = client.topics[str.encode(app_config["events"]["topic"])]
            trying = max_tries
        except:
            trying += 1
            logger.error("Could not connect to Kafka..")
            sleep(2.5)

    consumer = topic.get_simple_consumer(consumer_group=b'event_group', reset_offset_on_start=False,
                                         auto_offset_reset=OffsetType.LATEST)

    # This is blocking - it will wait for a new message
    for msg in consumer:
        try:
            msg_str = msg.value.decode('utf-8')
            msg = json.loads(msg_str)
            logger.info("Message: %s" % msg)

            payload = msg["payload"]

            if msg["type"] == "country":  # Change this to your event type - Get this from the openapi.yml line 13
                # Store the event1 (i.e., the payload) to the DB
                report_military_country_of_origin(payload)

            elif msg["type"] == "operation":  # Change this to your event type
                # Store the event2 (i.e., the payload) to the DB
                report_military_operation(payload)

            # Commit the new message as being read
            consumer.commit_offsets()
        except:
            logger.error("Something is wrong. Cannot Store in DB table")


app = connexion.FlaskApp(__name__, specification_dir='')
app.add_api(yaml_file,
            strict_validation=True,
            validate_responses=True)

if __name__ == "__main__":
    t1 = Thread(target=process_messages)
    t1.setDaemon(True)
    t1.start()

    app.run(port=8090)
