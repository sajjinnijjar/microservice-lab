from time import sleep

import connexion
from connexion import NoContent
import json, logging.config, requests, logging, yaml
from flask import Response
from pykafka import KafkaClient
from datetime import datetime
import datetime

# ? ASK where is the instruction said we have too add get endpoint to Receiver. Why we don't need it

with open('log_conf.yml', 'r') as f:
    log_config = yaml.safe_load(f.read())
    logging.config.dictConfig(log_config)

with open('app_conf.yml', 'r') as f:
    app_config = yaml.safe_load(f.read())
logger = logging.getLogger('basicLogger')

trying = 0
max_tries = int(app_config['events']['max_tries'])
while trying < max_tries:
    try:
        logger.info('Connecting to Kafka. Tries: {}'.format(trying))
        client = KafkaClient(hosts='{}:{}'.format(app_config['events']['hostname'], app_config['events']['port']))
        topic = client.topics[str.encode(app_config['events']['topic'])]
        trying = max_tries
    except:
        trying += 1
        logger.error("Could not connect to Kafka..")
        sleep(2.5)


def get_producer(body, type):
    producer = topic.get_sync_producer()
    msg = {"type": type, "datetime": datetime.datetime.now().strftime("%Y-%m-%dT%H:%M:%S"), "payload": body}
    msg_str = json.dumps(msg)
    producer.produce(msg_str.encode('utf-8'))


def post_military_country_of_origin(body):
    logger.info(f"Received event instore_sales request with a unique id of {body['military_id']}")
    get_producer(json.dumps(body), 'military_country')
    logger.info(f"military_country response Id: {body['military_id']} status: 200")
    return NoContent, 200


def post_military_operation(body):
    logger.info(f"Received event online_sales request with a unique id of {body['operation_id']}")
    get_producer(json.dumps(body), 'military_operation')
    logger.info("Returned event post_member response  ID:'{}' with status {}".format(body['operation_id'], 200))
    return NoContent, 200


app = connexion.FlaskApp(__name__, specification_dir='')
app.add_api('sajjin-military-1.0.0-resolved.yaml',
            strict_validation=True,
            validate_responses=True)

if __name__ == "__main__":
    app.run(port=8080)
